# devenv_init - Development environment initialization

## Short Decsription

- Role created for fast initialization of development environment
- Current release provides:
  - ssh_key and ssh_key.pub creating from variables
    - `WARNING!` - store keys securely with ansible-vault, sops, hc vault or another secret provider
  - .gitconfig configuration
    - `INFO` - sections are predefined (can be omitted), parameters can be any (according to [git documentation](https://git-scm.com/docs/git-config))

## Sample configuration

```yml
devenv_ssh_key_pub: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          39623234303737653961643835306139313636623964353262373238613433646233313339323835

devenv_ssh_key: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          39313432623164313364623562353338356333663765663230333032656537383434616462386632

devenv_git_user:
  name: "user"
  email: "user@email.domain"
  signingkey: "{{ ssh_config_dir }}/id_rsa"

devenv_git_gpg:
  format: "ssh"

devenv_git_commit:
  gpgsign: "true"

```
